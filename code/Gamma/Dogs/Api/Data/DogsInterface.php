<?php

namespace Gamma\Dogs\Api\Data;


interface DogsInterface
{
    const SUB = 'sub';
    const IMAGE = 'image';
    const BOOKS = 'books';
    const NAME = 'name';

    public function getName(): string;

    public function setName(string $name): DogsInterface;

    public function getImage(): string;

    public function setImage(string $image): DogsInterface;

    public function getSub(): array;

    public function setSub(array $sub): DogsInterface;

    public function getBooks(): array;

    public function setBooks(array $books): DogsInterface;

}