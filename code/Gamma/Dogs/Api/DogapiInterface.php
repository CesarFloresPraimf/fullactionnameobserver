<?php

namespace Gamma\Dogs\Api;


use Gamma\Dogs\Api\Data\DogsInterface;

interface DogapiInterface
{
    public function getDogs($name): DogsInterface;


}