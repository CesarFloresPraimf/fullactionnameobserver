<?php


namespace Gamma\Dogs\Model\Data;


use Gamma\Dogs\Api\Data\DogsInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Dogs extends AbstractSimpleObject implements DogsInterface
{
    public function getName(): string
    {
        return $this->_get(self::NAME);
    }

    public function setName(string $name): DogsInterface
    {
        return $this->setData(self::NAME, $name);
    }

    public function getImage(): string
    {
        return $this->_get(self::IMAGE);
    }

    public function setImage(string $image): DogsInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function getSub(): array
    {
        // TODO: Implement getSub() method.
        return $this->_get(self::SUB);
    }

    public function setSub(array $sub): DogsInterface
    {
        // TODO: Implement setSub() method.
        return $this->setData(self::SUB, $sub);
    }

    public function getBooks(): array
    {
        // TODO: Implement getBooks() method.
        return $this->_get(self::BOOKS);

    }

    public function setBooks(array $books): DogsInterface
    {
        // TODO: Implement setBooks() method.
        return $this->setData(self::BOOKS, $books);

    }
}
