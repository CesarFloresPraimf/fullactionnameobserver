<?php

namespace Gamma\PokeAPI\Observer;

//use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Request\Http;


class Pokedex implements ObserverInterface {
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger= $logger;
    }

    public function execute(Observer $observer){
        /** @var Http $request */
        $request = $observer->getEvent()->getData('request');

        $this->logger->info(
            __('Action name bla blablabal %1 asdfadsfadsf', $request->getFullActionName())
        );
        //To force a param
        //$request->setParam('pokemon', 'squirtle');
    }
}