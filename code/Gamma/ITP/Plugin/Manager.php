<?php

namespace Gamma\ITP\Plugin;

use Psr\Log\LoggerInterface;
use Magento\Framework\Event\ManagerInterface;

class Manager {

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function beforeDispatch(ManagerInterface $subject, $event, array $data = []){
        $this->logger->info(
            __('Event  dispatched %1', $event)
        );
    }
}