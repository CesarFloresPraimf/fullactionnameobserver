<?php


namespace Gamma\ITP\Model;


use Gamma\ITP\Api\Data\TestCaseInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class TestCase extends AbstractSimpleObject implements TestCaseInterface
{
    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->_get(self::ARGUMENTS);
    }

    /**
     * @param array $args
     * @return TestCaseInterface
     */
    public function setArguments(array $args): TestCaseInterface
    {
        return $this->setData(self::ARGUMENTS, $args);
    }

    /**
     * @return mixed
     */
    public function getExpected()
    {
        return $this->_get(self::EXPECTED_VALUE);
    }

    /**
     * @param $expected
     * @return TestCaseInterface
     */
    public function setExpected($expected): TestCaseInterface
    {
        return $this->setData(self::EXPECTED_VALUE, $expected);
    }

    /**
     * @return string
     */
    public function getFunctionName(): string
    {
        return $this->_get(self::FUNCTION_NAME);
    }

    /**
     * @param string $functionName
     * @return TestCaseInterface
     */
    public function setFunctionName(string $functionName): TestCaseInterface
    {
        return $this->setData(self::FUNCTION_NAME, $functionName);
    }

    /**
     * @return bool
     */
    public function getPassed(): bool
    {
        return $this->_get(self::PASSED);
    }

    /**
     * @param bool $passed
     * @return TestCaseInterface
     */
    public function setPassed(bool $passed): TestCaseInterface
    {
        return $this->setData(self::PASSED, $passed);
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->_get(self::RESULT);
    }

    /**
     * @param mixed $result
     * @return TestCaseInterface
     */
    public function setResult($result): TestCaseInterface
    {
        return $this->setData(self::RESULT, $result);
    }
}