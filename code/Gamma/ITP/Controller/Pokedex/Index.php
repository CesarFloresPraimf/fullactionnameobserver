<?php


namespace Gamma\ITP\Controller\Pokedex;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\Http;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var Http
     */
    protected $request;

    public function __construct(Context $context, PageFactory $pageFactory, Http $request)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->request = $request;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        $this->_eventManager->dispatch(
            'before_execute_pokedex',
            ['request' => $this->_request]
        );
        echo $this->request->getFullActionName();
        return $this->pageFactory->create();
    }
}